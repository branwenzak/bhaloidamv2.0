Bhaloidam-Handbook v2.0
=======================

v2.0 of the handbook will NOT be enitely in comic book format, unlike like the first version. I am considering some layout options that will make it easier to use the handbook as a quick reference. These layouts might also make it easier for non-visually-oriented people to read and understand it.

Please don't be shy about criticizing what I've written. I got relatively limited feedback on the first handbook and it would have been SO much better if I had the time and technology to gather more.